﻿using System;
using System.Data;
using Proyecto.Models;

namespace Proyecto.Data
{
    public class datDNA: dat
    {

        public void VERI_CREAR(entDNA objent)
        {
            try
            {
                dbCommand = db.GetStoredProcCommand("USP_VERI_CREAR");
                dbCommand.CommandTimeout = 120;

                db.AddInParameter(dbCommand, "DE_DNA", DbType.String, objent.DE_DNA);
                db.AddInParameter(dbCommand, "ST_MUTA", DbType.String, objent.ST_MUTA);
                db.ExecuteNonQuery(dbCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String VERI_LISTAR()
        {
            try
            {
                dbCommand = db.GetSqlStringCommand("SELECT * FROM TT_VERI");
                dbCommand.CommandTimeout = 120;

                DataTable DT = db.ExecuteDataSet(dbCommand).Tables[0];
                return ToJson_DT(DT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public String stats()
        {
            try
            {
                dbCommand = db.GetStoredProcCommand("USP_VERI_STATS");
                dbCommand.CommandTimeout = 120;

                DataTable DT = db.ExecuteDataSet(dbCommand).Tables[0];
                return ToJson_DT(DT);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
