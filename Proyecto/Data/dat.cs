﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using Newtonsoft.Json;

namespace Proyecto.Data
{
    public class dat
    {
        protected Database db = DatabaseFactory.CreateDatabase("dbreto");
        protected DbCommand dbCommand;

        public string ToJson_DT(DataTable DT)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(DT);
            return JSONString;
        }
    }
}
