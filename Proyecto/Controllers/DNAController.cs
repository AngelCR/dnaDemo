﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

//using CapaEntidad;
//using CapaDatos;
//using culqi.net;
using Newtonsoft.Json.Linq;
using Proyecto.Models;
using Proyecto.Data;

namespace Proyecto.Controllers
{
    public class DNAController : ApiController
    {
        entDNA objent;
        datDNA objdat = new datDNA();

        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

        [HttpPost]
        public HttpResponseMessage mutation([FromBody] List<String> listDNA)
        {
            try
            {
                if (!esMatrizValida(listDNA))
                {
                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", "No es Matriz"), System.Text.Encoding.UTF8, "application/json");
                    return response;
                }

                if (!caracteresValidos(listDNA))
                {
                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", "Cadena Incorrecta"), System.Text.Encoding.UTF8, "application/json");
                    return response;
                }

                String dna = "";
                foreach (String cadena in listDNA)
                {                    
                    dna = dna + cadena;
                    dna = dna + "-";
                }
                if (dna.EndsWith("-")){
                    dna = dna.Substring(0, dna.Length - 1);
                }
                objent = new entDNA
                {
                    DE_DNA = dna,
                    ST_MUTA = "N"
                };
                String Mensaje = "No se encontró Mutación";
                if (objent.ST_MUTA == "N" && secuenciaHorizontal(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Horizontal";
                }
                if (objent.ST_MUTA == "N" && secuenciaVertical(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Vertical";
                }
                if (objent.ST_MUTA == "N" && secuenciaOblicuaIzquierdaADerecha(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Oblicua";
                }
                if (objent.ST_MUTA == "N" && secuenciaOblicuaDerechaAIzquierda(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Oblicua";
                }

                objdat.VERI_CREAR(objent);

                response.Content = new StringContent(
                    String.Format("{{\"message\": \"{0}\"}}", Mensaje),
                    System.Text.Encoding.UTF8,
                    "application/json");
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", ex.Message), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage verificar(String dna)
        {
            try
            {
                List<string> listDNA = dna.Split('-').ToList();

                if (!esMatrizValida(listDNA))
                {
                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", "No es Matriz"), System.Text.Encoding.UTF8, "application/json");
                    return response;
                }

                if (!caracteresValidos(listDNA))
                {
                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", "Cadena Incorrecta"), System.Text.Encoding.UTF8, "application/json");
                    return response;
                }

                objent = new entDNA
                {
                    DE_DNA = dna,
                    ST_MUTA = "N"
                };
                String Mensaje = "No se encontró Mutación";
                if (objent.ST_MUTA == "N" && secuenciaHorizontal(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Horizontal";
                }
                if (objent.ST_MUTA == "N" && secuenciaVertical(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Vertical";
                }
                if (objent.ST_MUTA == "N" && secuenciaOblicuaIzquierdaADerecha(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Oblicua";
                }
                if (objent.ST_MUTA == "N" && secuenciaOblicuaDerechaAIzquierda(listDNA))
                {
                    objent.ST_MUTA = "S";
                    Mensaje = "Se encontró Mutación: *Oblicua";
                }

                response.Content = new StringContent(
                    String.Format("{{\"message\": \"{0}\"}}", Mensaje),
                    System.Text.Encoding.UTF8,
                    "application/json");
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.Forbidden;
                response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", ex.Message), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage stats()
        {
            try
            {
                response.Content = new StringContent(objdat.stats(), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", ex.Message), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }

        #region METODOS
        private Boolean esMatrizValida(List<String> dna)
        {
            int numFilas = dna.Count;

            if (numFilas < 4)
            {
                return false;
            }
            else
            {
                Boolean esMatriz = true;
                for (int i = 0; i < numFilas; i++)
                {
                    if (String.IsNullOrEmpty(dna[i]))
                    {
                        esMatriz = false;
                        break;
                    }

                    if (dna[i].Length != numFilas)
                    {
                        esMatriz = false;
                        break;
                    }
                }
                return esMatriz;
            }
        }

        private Boolean caracteresValidos(List<String> dna)
        {
            Boolean caracteresValidos = true;

            int ordenMatriz = dna.Count;

            for (int i = 0; i < ordenMatriz; i++)
            {
                for (int j = 0; j < ordenMatriz; j++)
                {
                    if (!(dna[i][j].Equals('A') || dna[i][j].Equals('T') || dna[i][j].Equals('C') || dna[i][j].Equals('G')))
                    {
                        caracteresValidos = false;
                        break;
                    }
                }

                if (!caracteresValidos)
                {
                    break;
                }
            }
            return caracteresValidos;
        }

        private Boolean secuenciaHorizontal(List<String> dna)
        {
            Boolean mutacionEncontrada = false;

            int ordenMatriz = dna.Count;
            String secuencia;
            for (int i = 0; i < ordenMatriz; i++)
            {
                secuencia = "";
                for (int j = 0; j <= (ordenMatriz - 4); j++)
                {
                    secuencia = String.Concat(dna[i][j], dna[i][j + 1], dna[i][j + 2], dna[i][j + 3]);

                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                }

                if (mutacionEncontrada)
                {
                    break;
                }
            }
            return mutacionEncontrada;
        }

        private Boolean secuenciaVertical(List<String> dna)
        {
            Boolean mutacionEncontrada = false;

            int ordenMatriz = dna.Count;
            String secuencia;

            for (int j = 0; j < ordenMatriz; j++)
            {
                secuencia = "";
                for (int i = 0; i <= (ordenMatriz - 4); i++)
                {
                    secuencia = String.Concat(dna[i][j], dna[i + 1][j], dna[i + 2][j], dna[i + 3][j]);
                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                }

                if (mutacionEncontrada)
                {
                    break;
                }
            }
            return mutacionEncontrada;
        }

        private Boolean secuenciaOblicuaIzquierdaADerecha(List<String> dna)
        {
            Boolean mutacionEncontrada = false;

            int ordenMatriz = dna.Count;
            String secuencia;
            int i, j;
            //CENTRAL
            for (int n = 0; n <= (ordenMatriz - 4); n++)
            {
                i = n;
                j = n;
                secuencia = String.Concat(dna[i][j], dna[i + 1][j + 1], dna[i + 2][j + 2], dna[i + 3][j + 3]);
                if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                {
                    mutacionEncontrada = true;
                    break;
                }
            }
            if (mutacionEncontrada)
            {
                return mutacionEncontrada;
            }

            if (ordenMatriz <= 4)
            {
                return mutacionEncontrada;
            }

            //INFERIOR Y SUPERIOR     
            for (int m = 1; m <= (ordenMatriz - 4); m++)
            {
                for (int n = 0; n <= (ordenMatriz - 4 - m); n++)
                {
                    // INFERIOR
                    i = m;
                    j = n;
                    secuencia = String.Concat(dna[i][j], dna[i + 1][j + 1], dna[i + 2][j + 2], dna[i + 3][j + 3]);
                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                    // SUPERIOR
                    i = n;
                    j = m;
                    secuencia = String.Concat(dna[i][j], dna[i + 1][j + 1], dna[i + 2][j + 2], dna[i + 3][j + 3]);
                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                }
                if (mutacionEncontrada)
                {
                    break;
                }
            }

            return mutacionEncontrada;
        }

        private Boolean secuenciaOblicuaDerechaAIzquierda(List<String> dna)
        {
            Boolean mutacionEncontrada = false;

            int ordenMatriz = dna.Count;
            String secuencia;
            int i, j;
            //CENTRAL
            for (int n = 0; n <= (ordenMatriz - 4); n++)
            {
                i = ordenMatriz - n - 1;
                j = n;
                secuencia = String.Concat(dna[i][j], dna[i - 1][j + 1], dna[i - 2][j + 2], dna[i - 3][j + 3]);
                if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                {
                    mutacionEncontrada = true;
                    break;
                }
            }
            if (mutacionEncontrada)
            {
                return mutacionEncontrada;
            }

            if (ordenMatriz <= 4)
            {
                return mutacionEncontrada;
            }

            //INFERIOR Y SUPERIOR     
            for (int m = 1; m <= (ordenMatriz - 4); m++)
            {
                for (int n = 0; n <= (ordenMatriz - 4 - m); n++)
                {
                    // INFERIOR
                    i = ordenMatriz - n + 1;
                    j = m + n;
                    secuencia = String.Concat(dna[i][j], dna[i - 1][j + 1], dna[i - 2][j + 2], dna[i - 3][j + 3]);
                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                    // SUPERIOR
                    i = ordenMatriz - n - m - 1;
                    j = n;
                    secuencia = String.Concat(dna[i][j], dna[i - 1][j + 1], dna[i - 2][j + 2], dna[i - 3][j + 3]);
                    if (secuencia.Equals("AAAA") || secuencia.Equals("TTTT") || secuencia.Equals("CCCC") || secuencia.Equals("GGGG"))
                    {
                        mutacionEncontrada = true;
                        break;
                    }
                }
                if (mutacionEncontrada)
                {
                    break;
                }
            }

            return mutacionEncontrada;
        }
        #endregion

        [HttpGet]
        public HttpResponseMessage consulta()
        {
            try
            {
                response.Content = new StringContent(objdat.VERI_LISTAR(), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent(String.Format("{{\"message\": \"{0}\"}}", ex.Message), System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }
    }
}
